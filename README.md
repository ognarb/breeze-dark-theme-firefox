# Dark theme for firefox (breeze dark)

This project provides a userContent.css which enable a dark theme for certain website.

## Screenshot

![gitlab](./screenshot/gitlab.png)

## Contributing 

If you want find a bug for one of the provided webpage, a merge request or an issue is wellcome. If you want to add a new webpage, a merge request is wellcome, but I don't accept feature request.  

## Licence

This project is licenced under GPL3. No warranty is provided
